mod bot;
mod error;
mod models;
mod urls;

use std::{env, io::Write, process::exit};

use bot::{Bot, Desire, Product};
use chrono::{NaiveDateTime, Utc};
use clap::{Parser, ValueEnum};
use colored::Colorize;
use error::{BotError, BotResult};
use tracing::{debug, error, info, warn};
use tracing_subscriber::{
    filter::LevelFilter, fmt, prelude::__tracing_subscriber_SubscriberExt, reload,
    util::SubscriberInitExt,
};

#[derive(Debug, Clone, ValueEnum)]
enum Mode {
    Single,
    Multi,
}

/// A bot to help you get tickets on ticketplus.com.tw.
/// Written by WuuBoLn.
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Mode to run on
    #[arg(short, long)]
    mode: Option<Mode>,

    /// The time for execution in UNIX timestamp
    #[arg(short, long)]
    time: Option<i64>,

    /// Generate a .env and a settings json template
    #[arg(long)]
    gen: bool,

    /// Generate a .env template
    #[arg(long)]
    gen_env: bool,

    /// Generate a settings json template
    #[arg(long)]
    gen_json: bool,

    /// Turn on debug mode
    #[arg(long)]
    debug: bool,

    /// [DANGEROUS] Release all reserved orders; this action cannot be reverted
    #[arg(long)]
    release_all_reserved_orders: bool,
}

#[tokio::main]
async fn main() -> BotResult<()> {
    #[cfg(not(debug_assertions))]
    std::panic::set_hook(Box::new(|_| {
        println!("ticketplus-bot: Something went wrong...");
    }));

    let args = Args::parse();

    let level = if args.debug {
        LevelFilter::DEBUG
    } else {
        LevelFilter::INFO
    };

    let (filter, _reload_handle) = reload::Layer::new(level);
    tracing_subscriber::registry()
        .with(filter)
        .with(fmt::Layer::default().with_target(false))
        .init();

    dotenv::dotenv().ok();

    parse_args(&args);

    let (Ok(country_code), Ok(mobile), Ok(password)) = (
        env::var("COUNTRY_CODE"),
        env::var("MOBILE"),
        env::var("PASSWORD"),
    ) else {
        error!("Env setup incorrectly. Use `--gen-env` to generate a .env template.");
        return Ok(());
    };

    let Ok(country_code) = country_code.parse::<i32>() else {
        error!("`COUNTRY_CODE` should be three digit number");
        return Ok(());
    };

    let Ok(settings) = std::fs::read_to_string("./settings.json") else {
        error!(
            "Cannot find `settings.json`. Use `--gen-json` to generate a settings json` template"
        );
        return Ok(());
    };

    let desire = serde_json::from_str(&settings)?;
    let mut bot = Bot::new(&country_code, &mobile, &password, &desire).await?;

    if args.release_all_reserved_orders {
        let mut input = String::new();
        println!();
        print!(
            "{} {} {}",
            "Releasing orders".red(),
            "cannot".red().bold(),
            "be reverted. Proceed? ".red()
        );
        std::io::stdout().flush().unwrap();
        std::io::stdin().read_line(&mut input).unwrap();
        if input.to_lowercase() != "y\n"
            && input.to_lowercase() != "ye\n"
            && input.to_lowercase() != "yes\n"
        {
            debug!("User entered: {:#?}", input);
            error!("Aborted!!!");
            exit(0);
        }

        warn!("Releasing all reserved orders...");
        let orders = bot.get_all_reserved_orders().await?.reserved_data;
        for order in orders {
            bot.release(order.order_id.parse::<usize>().unwrap())
                .await?;
        }
        info!("Orders released")
    } else if !bot.no_exist_reserved_orders().await? {
        error!("Abort! Currently have some reserved orders. Use `--release-all-reserved-orders` to release them");
        return Ok(());
    }

    let mode = args.mode.unwrap_or(Mode::Single);

    if let Some(timestamp) = args.time {
        let current_timestamp = Utc::now().timestamp_nanos_opt().unwrap();
        let target_timestamp_nanos = timestamp * 1_000_000_000;

        if target_timestamp_nanos - current_timestamp <= 0 {
            error!("The target timestamp has already passed");
            exit(0);
        }

        let duration_until_target =
            chrono::Duration::nanoseconds(target_timestamp_nanos - current_timestamp);
        let sleep_duration = std::time::Duration::from_nanos(
            duration_until_target.num_nanoseconds().unwrap_or(0) as u64,
        );

        let datetime = NaiveDateTime::from_timestamp_opt(timestamp, 0).unwrap();
        info!(
            "Waiting until {} {}...",
            datetime
                .format("%Y-%m-%d %H:%M:%S")
                .to_string()
                .purple()
                .italic(),
            "UTC".purple().italic()
        );

        std::thread::sleep(sleep_duration);
    }

    match mode {
        Mode::Single => run_single(&mut bot).await?,
        Mode::Multi => run_multi(&mut bot).await?,
    }

    if !bot
        .get_all_reserved_orders()
        .await?
        .reserved_data
        .is_empty()
    {
        info!("{}", "TICKETS RESERVED SUCCESSFULLY".cyan().bold());
    }

    Ok(())
}

async fn run_single(bot: &mut Bot) -> BotResult<()> {
    let mut ok = false;

    while !ok {
        for i in 0..bot.desire.products.len() {
            if !matches!(bot.reserve(Some(i)).await, Err(BotError::TicketsSoldOut)) {
                ok = true;
                break;
            }
        }
    }

    Ok(())
}

async fn run_multi(bot: &mut Bot) -> BotResult<()> {
    loop {
        if !matches!(bot.reserve(None).await, Err(BotError::TicketsSoldOut)) {
            break Ok(());
        }
    }
}

fn parse_args(args: &Args) {
    if args.gen {
        gen_json();
        gen_env();
        debug!("Wrote env and settings file, exiting...");
        exit(0);
    } else if args.gen_json {
        gen_json();
        debug!("Wrote settings file, exiting...");
        exit(0);
    } else if args.gen_env {
        gen_env();
        debug!("Wrote env file, exiting...");
        exit(0);
    }
}

fn gen_env() {
    let file_name = ".env";
    let data = r#"COUNTRY_CODE=886
MOBILE=987654321
PASSWORD=verysuperpassword"#;

    std::fs::File::create(file_name)
        .unwrap()
        .write_all(data.as_bytes())
        .unwrap();
}

fn gen_json() {
    let file_name = "settings.json";
    debug!("Writing {}...", file_name);
    let desire = Desire {
        session_id: "".to_owned(),
        products: vec![Product {
            id: "".to_owned(),
            count: 1,
        }],
    };
    std::fs::File::create(file_name)
        .unwrap()
        .write_all(
            serde_json::to_string(&desire)
                .unwrap()
                .into_bytes()
                .as_ref(),
        )
        .unwrap();
}
