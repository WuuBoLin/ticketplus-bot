pub type BotResult<T> = Result<T, BotError>;

#[derive(Debug, thiserror::Error)]
pub enum BotError {
    #[error("Failed to send the api request")]
    RequestError(#[from] reqwest::Error),
    #[error("Failed to send the api request")]
    SerdeJsonError(#[from] serde_json::Error),
    #[error("Unknown response")]
    UnknownResponse,
    #[error("Access token expired")]
    AccessTokenExpired,
    #[error("Index out of bounds")]
    IndexOutOfBounds,
    #[error("Tickets sold out")]
    TicketsSoldOut,
    #[error("Unable to reserve tickets")]
    ReserveTicketsFailed,
    #[error("Unable to release orders")]
    ReleaseOrdersFailed,
}
