use std::fmt::Display;

pub const LOGIN_URL: &str = "https://apis.ticketplus.com.tw/user/api/v1/login";
pub const CAPTCHA_GEN_URL: &str = "https://apis.ticketplus.com.tw/captcha/api/v1/generate";
pub const RESERVE_URL: &str = "https://apis.ticketplus.com.tw/ticket/api/v1/reserve";
pub const REFRESH_TOKEN_URL: &str = "https://apis.ticketplus.com.tw/user/api/v1/refreshToken";
pub const ALL_ORDERS_URL: &str = "https://apis.ticketplus.com.tw/order/api/v1/getAllOrders";
pub const RELEASE_URL: &str = "https://apis.ticketplus.com.tw/ticket/api/v1/release";

pub enum Urls {
    Login,
    Captcha,
    Reserve,
    RefreshToken,
    AllOrders,
    Release,
}

impl Display for Urls {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Login => f.write_str(LOGIN_URL),
            Self::Captcha => f.write_str(CAPTCHA_GEN_URL),
            Self::Reserve => f.write_str(RESERVE_URL),
            Urls::RefreshToken => f.write_str(REFRESH_TOKEN_URL),
            Urls::AllOrders => f.write_str(ALL_ORDERS_URL),
            Urls::Release => f.write_str(RELEASE_URL),
        }
    }
}
