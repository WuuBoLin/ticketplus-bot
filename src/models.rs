use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct UserInfo {
    pub access_token: String,
    pub access_token_expires_in: u64,
    pub id: String,
    pub refresh_token: String,
    #[serde(rename = "verifyEmail")]
    pub verify_email: bool,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct LoginResponse {
    pub err_code: String,
    pub err_detail: String,
    pub err_msg: String,
    pub user_info: UserInfo,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CaptchaResponse {
    pub err_code: String,
    pub err_msg: String,
    pub err_detail: String,
    pub key: String,
    pub data: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ReserveResponse {
    pub err_code: String,
    pub err_msg: String,
    pub err_detail: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AllOrdersResponse {
    pub data: Vec<serde_json::Value>,
    pub err_code: String,
    pub err_detail: String,
    pub err_msg: String,
    pub reserved_data: Vec<ReservedData>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ReservedData {
    pub amount: f64,
    pub coupon_content: String,
    pub culture_point_deduct_amount: f64,
    pub discount: f64,
    pub event_id: String,
    pub has_applicant: bool,
    pub order_event_big_thumbnail: String,
    pub order_event_small_thumbnail: String,
    pub order_id: String,
    pub session_date_time: String,
    pub session_id: String,
    pub session_location: String,
    pub session_name: String,
    pub tickets: Vec<Ticket>,
    pub total: f64,
    pub total_amount: f64,
    pub total_count: f64,
    pub trans_type: String,
    pub user_id: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Ticket {
    pub applicant: serde_json::Value,
    pub count: f64,
    pub culture_point_ticket_deduct_amount: f64,
    pub discount_name: String,
    pub price: f64,
    pub product_id: String,
    pub product_name: String,
    pub seat_assignment: bool,
    pub seat_info: serde_json::Value,
    pub single_price: f64,
    pub single_ticket_price: f64,
    pub ticket_area_name: String,
    pub un_countable: bool,
}
