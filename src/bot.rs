use std::{cell::RefCell, fmt::Display, rc::Rc};

use crate::{
    error::*,
    models::{AllOrdersResponse, CaptchaResponse, LoginResponse, ReserveResponse},
    urls::*,
};
use once_cell::sync::Lazy;
use reqwest::{header::*, Client, ClientBuilder};
use resvg::{
    tiny_skia::{self, Color},
    usvg::{self, fontdb, TreeParsing, TreeTextToPath},
};
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use serde_json::{json, Value};
use tracing::{debug, error, info, warn};

pub static HTTP: Lazy<Client> = Lazy::new(|| {
    ClientBuilder::new()
        .cookie_store(true)
        .use_rustls_tls()
        .default_headers(HEADERS.clone())
        .build()
        .unwrap()
});

pub static HEADERS: Lazy<HeaderMap> = Lazy::new(|| {
    let mut map = HeaderMap::new();
    map.insert(
        USER_AGENT,
        HeaderValue::from_static(
            "Mozilla/5.0 (X11; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0",
        ),
    );
    map.insert(
        ACCEPT,
        HeaderValue::from_static("application/json, text/plain, */*"),
    );
    map.insert(ACCEPT_LANGUAGE, HeaderValue::from_static("en-US,en;q=0.5"));
    map.insert(
        ACCEPT_ENCODING,
        HeaderValue::from_static("gzip, deflate, br"),
    );
    map.insert(CONTENT_TYPE, HeaderValue::from_static("application/json"));
    map.insert(
        ORIGIN,
        HeaderValue::from_static("https://ticketplus.com.tw"),
    );
    map.insert(
        REFERER,
        HeaderValue::from_static("https://ticketplus.com.tw/"),
    );
    map.insert(PRAGMA, HeaderValue::from_static("no-cache"));
    map.insert(CACHE_CONTROL, HeaderValue::from_static("no-cache"));
    map.insert(TE, HeaderValue::from_static("trailers"));
    map
});

pub enum Method {
    Get,
    Post,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Product {
    pub id: String,
    pub count: usize,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Desire {
    pub session_id: String,
    pub products: Vec<Product>,
}

#[derive(Debug)]
pub struct Bot {
    pub user_id: String,
    pub token: String,
    pub refresh_token: String,
    pub desire: Desire,
    pub captcha: Option<Rc<RefCell<String>>>,
}

impl Bot {
    pub async fn new<I: Into<String> + Serialize + Display>(
        country_code: &i32,
        account: &I,
        password: &I,
        desire: &Desire,
    ) -> BotResult<Self> {
        let md5_password =
            format!("{:X}", md5::compute(password.to_string().as_bytes())).to_lowercase();

        let res: LoginResponse = loop {
            info!("SignIn with mobile {}...", account);

            let res = HTTP
                .post(Urls::Login.to_string())
                .json(&json!({
                    "countryCode": country_code.to_string(),
                    "mobile": account,
                    "password": md5_password,
                }))
                .send()
                .await?
                .json::<Value>()
                .await?;

            if res["errCode"] != "00" {
                error!("SignIn Failed!!");
                debug!("SignIn Response: {:#?}", res);
                continue;
            }

            break serde_json::from_value::<LoginResponse>(res)?;
        };

        let user = res.user_info;

        info!("Signed IN!!");

        Ok(Bot {
            user_id: user.id,
            token: user.access_token,
            refresh_token: user.refresh_token,
            desire: desire.clone(),
            captcha: None,
        })
    }

    async fn request_with_auth<T: DeserializeOwned>(
        &mut self,
        method: Method,
        url: Urls,
        body: Option<&Value>,
    ) -> BotResult<T>
    where
        T: DeserializeOwned,
    {
        loop {
            info!("Sending Request...");

            let response_json = match method {
                Method::Get => {
                    HTTP.get(url.to_string())
                        .bearer_auth(&self.token)
                        .send()
                        .await?
                        .json::<Value>()
                        .await?
                }
                Method::Post => {
                    HTTP.post(url.to_string())
                        .bearer_auth(&self.token)
                        .json(body.unwrap_or(&json!({})))
                        .send()
                        .await?
                        .json::<Value>()
                        .await?
                }
            };

            info!("Request Sent");
            debug!("Response: {:#?}", response_json);

            let Ok(response) = serde_json::from_value::<T>(response_json.clone()) else {
                error!("Unknown Response!!");
                break Err(BotError::UnknownResponse);
            };

            if response_json["errCode"].is_null() {
                warn!("No `errCode` found");
                return Ok(response);
            }

            if response_json["errCode"] == "103" {
                error!("Access token expired");

                info!("Refreshing access token...");

                let res = HTTP
                    .post(Urls::RefreshToken.to_string())
                    .bearer_auth(&self.token)
                    .json(&json!({}))
                    .send()
                    .await?
                    .json::<LoginResponse>()
                    .await?;

                debug!("Response: {:#?}", response_json);

                if res.err_code == "00" {
                    self.token = res.user_info.access_token;
                    info!("Access token refreshed.")
                } else {
                    error!("Unable to refresh access token");
                    break Err(BotError::AccessTokenExpired);
                }

                continue;
            }

            return Ok(response);
        }
    }

    pub async fn solve_captcha(&mut self) -> BotResult<&mut Self> {
        info!("Fetching a new captcha image...");

        let res: CaptchaResponse = self
            .request_with_auth(
                Method::Post,
                Urls::Captcha,
                Some(&json!({
                    "sessionId": self.desire.session_id,
                    "refresh": true,
                })),
            )
            .await?;

        info!("Solving Captcha...");

        let rtree = {
            let opt = usvg::Options::default();
            let mut fontdb = fontdb::Database::new();
            fontdb.load_system_fonts();

            let mut tree = usvg::Tree::from_str(&res.data, &opt).unwrap();
            tree.convert_text(&fontdb);
            resvg::Tree::from_usvg(&tree)
        };

        let pixmap_size = rtree.size.to_int_size();
        let mut pixmap = tiny_skia::Pixmap::new(pixmap_size.width(), pixmap_size.height()).unwrap();
        pixmap.fill(Color::WHITE);
        rtree.render(tiny_skia::Transform::default(), &mut pixmap.as_mut());

        let mut ocr = ddddocr::ddddocr_classification().unwrap();
        let res = ocr.classification(pixmap.encode_png().unwrap()).unwrap();

        info!("Captcha guess: {}", res);

        self.captcha = Some(Rc::new(RefCell::new(res)));

        Ok(self)
    }

    pub async fn reserve(&mut self, products_index: Option<usize>) -> BotResult<ReserveResponse> {
        let products: Vec<Value>;
        if let Some(products_index) = products_index {
            let Some(product) = self.desire.products.get(products_index) else {
                error!("Product index out of bounds");
                return Err(BotError::IndexOutOfBounds);
            };
            products = vec![json!({
            "productId": product.id,
            "count": product.count,
            })];
        } else {
            products = self
                .desire
                .products
                .iter()
                .map(|product| {
                    json!({
                        "productId": product.id,
                        "count": product.count,
                    })
                })
                .collect::<Vec<_>>();
        }

        if self.captcha.is_none() {
            self.solve_captcha().await?;
        }

        loop {
            info!("Reserving tickets...");

            let res: ReserveResponse = self
                .request_with_auth(
                    Method::Post,
                    Urls::Reserve,
                    Some(&json!({
                    "products": products,
                    "captcha": {
                        "key": self.user_id,
                        "ans": *self.captcha.clone().unwrap(),
                    }
                    })),
                )
                .await?;

            match res.err_code.as_str() {
                "137" => {
                    // is not hit
                    warn!("Reserve[{}]: {}", res.err_code, res.err_detail);
                    continue;
                }
                "135" | "136" => {
                    // Captcha failed, captcha not found
                    error!("Reserve[{}]: {}", res.err_code, res.err_detail);
                    self.solve_captcha().await?;
                    continue;
                }
                "115" => {
                    // Tickets sold out
                    error!("Reserve[{}]: {}", res.err_code, res.err_detail);
                    break Err(BotError::TicketsSoldOut);
                }
                "111" => {
                    // currently has reserved order
                    info!("Reserve[{}]: {}", res.err_code, res.err_detail);
                    break Ok(res);
                }
                "00" => {
                    // SUCCESS
                    info!("Reserve[{}]: SUCCESS", res.err_code);
                    break Ok(res);
                }
                &_ => {
                    error!("Reserve[{}]: {}", res.err_code, res.err_detail);
                    break Err(BotError::ReserveTicketsFailed);
                }
            }
        }
    }

    pub async fn no_exist_reserved_orders(&mut self) -> BotResult<bool> {
        Ok(self
            .get_all_reserved_orders()
            .await?
            .reserved_data
            .is_empty())
    }

    pub async fn get_all_reserved_orders(&mut self) -> BotResult<AllOrdersResponse> {
        info!("Getting all of your reserved orders...");

        let res = self
            .request_with_auth::<AllOrdersResponse>(Method::Get, Urls::AllOrders, None)
            .await?;

        if res.reserved_data.is_empty() {
            info!("Currently has {} orders reserved.", res.reserved_data.len());
            return Ok(res);
        }

        warn!("Currently has {} orders reserved.", res.reserved_data.len());

        let orders = res.reserved_data.iter().fold(String::new(), |acc, d| {
            acc + &format!("[{} x{}]", d.session_name, d.tickets.len())
        });

        warn!("Reserved Orders: {}", orders);

        Ok(res)
    }

    pub async fn release(&mut self, order_id: usize) -> BotResult<()> {
        warn!("Releasing order {}...", order_id);

        let res = self
            .request_with_auth::<Value>(
                Method::Post,
                Urls::Release,
                Some(&json!({
                        "orderId": order_id
                })),
            )
            .await?;

        if res["errCode"] != "00" {
            return Err(BotError::ReleaseOrdersFailed);
        }

        info!("Order {} released", order_id);

        Ok(())
    }
}
